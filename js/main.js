$(window).load(function() {
  	$('.flexslider').flexslider({
  	  	animation: "fade",
  	  	smoothHeight: true,
  	  	controlNav: false,
  	  	directionNav: false,
  	  	animationSpeed: 1000
  	});
});
